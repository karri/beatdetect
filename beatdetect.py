import sfml
from math import *
import time

'''
    The graphics of a rotating wheel
'''
class wheel:
    ''' Read in the graphics '''
    def __init__(self, rpm):
        WHEEL = 0
        self.textureHolder = {
            WHEEL    : sfml.graphics.Texture.from_file("wheels.png")
        }
        self.width = 50
        self.height = 223
        self.Frames = [sfml.graphics.Rectangle((self.width*i, 0), (self.width, self.height)) for i in range(6)]
        self.Sprite = sfml.graphics.Sprite(self.textureHolder[WHEEL])
        self.Sprite.color = sfml.graphics.Color(255, 255, 255, 200)
        ''' The starttime of the wheel in ms. Well, it is now '''
        self.starttime = self.timenow()
        self.setrpm(rpm)

    ''' Set the rpm of the wheel '''
    def setrpm(self, rpm):
        self.rpm = rpm
        ''' The time for one beat of this wheel '''
        self.beattime = 60.0 * 1000.0 / self.rpm

    ''' The time now in ms '''
    def timenow(self):
        return int(time.time() * 1000)

    ''' Draw the tractor wheel. With the light if on the beat '''
    def drawsprite(self):
        duration = self.timenow() - self.starttime
        beats = duration / self.beattime
        leftover = duration % self.beattime
        if leftover < self.beattime/2:
            light = 1
        else:
            light = 0
        self.Sprite.texture_rectangle = self.Frames[(int(beats) % 3) + 3 * light]
        return(self.Sprite)


'''
    The math for finding out where the sound hits hit the rotating wheel
'''
class yamartino:
    ''' We init the values for the math at start time '''
    def __init__(self, rpm):
        ''' The rpm speed of this wheel '''
        self.rpm = rpm
        ''' Duration for 4 rotations '''
        self.duration = int(1000 * 4 * 60.0 / self.rpm)
        ''' Degrees per sec '''
        self.degpersec = 4 * 360.0 / self.duration
        ''' The starttime of the wheel in ms. Well, it is now '''
        self.starttime = self.timenow()
        self.endtime = self.starttime + self.duration
        self.cleanup()
        self.eps = 0
        self.calcangle = 0
        self.newdata = 0

    ''' Get the rpm of this wheel '''
    def getrpm(self):
        return self.rpm

    def cleanup(self):
        ''' The yamartino method collects the sin and cos values of the measurements '''
        self.s=0
        self.c=0
        ''' Number of measurements so far '''
        self.n = 0

    ''' The time now in ms '''
    def timenow(self):
        return int(time.time() * 1000)

    ''' Check end of duration calculations '''
    def checkend(self):
        if self.timenow() >= self.endtime:
            if self.n >= 4:
                s=self.s/self.n
                c=self.c/self.n
                self.eps=(1-(s**2+c**2))**0.5
                try:
                    self.calcangle = degrees(asin(eps)*(1+(2.0/3.0**0.5-1)*eps**3))
                except:
                    pass
                self.newdata = 1
            self.cleanup()
            self.starttime = self.timenow()
            self.endtime = self.starttime + self.duration

    ''' Calculate the current angle of the wheel '''
    def anglenow(self):
        self.angle = ((self.timenow() - self.starttime) * self.degpersec) % 360
        return self.angle

    ''' Now we get a hit '''
    def shoot(self):
        anglenow = self.anglenow()
        ''' Add the sin and cos values to the yamartino model '''
        self.s=self.s+sin(radians(anglenow))
        self.c=self.c+cos(radians(anglenow))
        self.n = self.n + 1
        self.checkend()

    def getnewdata(self):
        return self.newdata

    def geteps(self):
        return self.eps

    def getcalcangle(self):
        self.newdata = 0
        return self.calcangle


'''
    The beat detector application without graphics or animation
'''
class detector:
    ''' We initialize a range of yamartino rpm detectors '''
    def __init__(self):
        self.d = {}
        for rpm in range(100, 220):
            self.d[rpm] = yamartino(rpm)
        self.best = 0
        self.smallest_eps = 1.0

    ''' Now we get a hit. Pass it to all yamartino wheels '''
    def shoot(self):
        for rpm in self.d:
            self.d[rpm].shoot()
        self.bestrpm()

    ''' Find the best rpm match '''
    def bestrpm(self):
        best = 0
        smallest_eps = 1.0
        for rpm in self.d:
            if self.d[rpm].getnewdata() == 1:
                eps = self.d[rpm].geteps()
                if eps < smallest_eps:
                    smallest_eps = eps
                    best = rpm
        if self.best != best:
            self.best = best
            self.smallest_eps = smallest_eps

    ''' Get best rpm match '''
    def getbestrpm(self):
        return self.best

    ''' Get best eps '''
    def getbesteps(self):
        return self.smallest_eps


'''
    The beat detector application
'''
playerFrame = 0
# create the main window
window = sfml.graphics.RenderWindow(sfml.window.VideoMode(200, 300), "Wheels Test")

rpm = 160
prevrpm = rpm
# start the game loop
w = wheel(rpm)
d = detector()
playerSprite = w.drawsprite()
playerSprite.position = (80, 20)
lastdisplay = time.time() * 1000 + 50
while window.is_open:
    # process events
    for event in window.events:
        if type(event) is sfml.window.KeyEvent and event.pressed and event.code is sfml.window.Keyboard.SPACE:
            d.shoot()
        # close window: exit
        if type(event) is sfml.window.CloseEvent:
            window.close()
    if time.time() * 1000 > lastdisplay :
        lastdisplay = time.time() * 1000 + 20
        rpm = d.getbestrpm()
        eps = d.getbesteps()
        if rpm > 0 and rpm != prevrpm:
            prevrpm = rpm
            print(rpm)
            w.setrpm(rpm)
        playerSprite = w.drawsprite()

    window.clear()   # clear screen
    window.draw(playerSprite)   # draw the sprite
    window.display()   # update the window
